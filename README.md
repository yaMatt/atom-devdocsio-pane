# devdocio-pane package

Open word under cursor in devdocs.io pane. Not affiliated with the wonderful people at devdocs.io.

## Usage
* Select word to search for
* Press ctrl+alt+d or cmd+alt+d on macOS
