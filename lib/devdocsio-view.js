'use babel';

export default class DevdocsioView {

  DEVDOCS_URL=new URL("https://devdocs.io");

  constructor(serializedState) {
    // Create root element
    this.element = document.createElement('iframe');
    this.element.src = this.create_url();
    this.element.classList.add("devdocsio");
  }

  create_url() {
    var wordToSearchFor = atom.workspace.getActiveTextEditor().getSelectedText() || atom.workspace.getActiveTextEditor().getWordUnderCursor();
    var grammar = atom.workspace.getActiveTextEditor().getGrammar().name;
    return "/#q=" + grammar + " " + wordToSearchFor;
  }

  set_url(url) {
    if (url === undefined) {
      url = this.create_url()
    }
    var new_url = new URL(url, this.DEVDOCS_URL,);
    this.element.src = new_url.href;
  }

  // Returns an object that can be retrieved when package is activated
  serialize() {
    return JSON.serialize({
      "url": this.element.src
    });
  }

  // Tear down any state and detach
  destroy() {
    this.element.remove();
  }

  getElement() {
    return this.element;
  }

}
