'use babel';

import DevdocsioView from './devdocsio-view';
import { CompositeDisposable } from 'atom';

export default {

  devdocsioView: null,
  panel: null,
  subscriptions: null,

  activate(state) {
    this.devdocsioView = new DevdocsioView(state.devdocsioViewState);
    this.devdocsioView.set_url();
    this.panel = atom.workspace.addRightPanel({
      item: this.devdocsioView.getElement(),
      visible: false
    });

    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();

    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'devdocsio:search': () => this.search(),
      'devdocsio:close': () => this.close()
    }));
  },

  deactivate() {
    this.panel.destroy();
    this.subscriptions.dispose();
    this.devdocsioView.destroy();
  },

  serialize() {
    return {
      devdocsioViewState: this.devdocsioView.serialize()
    };
  },

  search() {
    if (!this.panel.isVisible()) {
      this.panel.show()
    }
    else {
      this.devdocsioView.set_url();
    }
  },
  close() {
    this.panel.hide()
  }

};
